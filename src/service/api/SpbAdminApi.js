import axios from "axios";

const spbAdminApi = () => {
  const defaultOptions = {
    baseURL: "http://localhost:3013/",
  headers: {
    "Content-type": "application/json",
  },
};

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('app_authKey');
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    return config;
  });

  return instance;
};

export default spbAdminApi();