import spbKkt from "./api/SpbKktApi";
class LoginService {
  getAll() {
    return spbKkt.get("/tutorials");
  }
  get(id) {
    return spbKkt.get(`/tutorials/${id}`);
  }
  signViaAdminSc(data) {
    return spbKkt.post("adm/auth/signviaadminsc", data);
  }
  update(id, data) {
    return spbKkt.put(`/tutorials/${id}`, data);
  }
  delete(id) {
    return spbKkt.delete(`/tutorials/${id}`);
  }
  deleteAll() {
    return spbKkt.delete(`/tutorials`);
  }
  findByTitle(title) {
    return spbKkt.get(`/tutorials?title=${title}`);
  }
}
export default new LoginService();