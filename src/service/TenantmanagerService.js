import spbKkt from "./api/SpbKktApi";
class TenantmanagerService{
    retriveTenantByNamein(data){
        return spbKkt.post('adm/tenants/getAllTenantsIn', data);
    }
}
export default new TenantmanagerService();
