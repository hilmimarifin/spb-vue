import spborder from "./api/SpbOrderApi";
class PlanningOrder {
    getAlldata() {
      return spborder.get("/spb/spb_planningorder/getalldata");
    }
    getTableData() {
      return spborder.get("/spb/spb_planningorder/tableopenreq");
    }
    GetTableNested() {
      return spborder.get("/spb/spb_planningorder/getnestedbaru");
    }
    getTableSearch(data) {
      return spborder.get(`/spb/spb_planningorder/tableopenreq/`+ data)
    }
    // GetTableSearch(data) {
    //   return spborder.get("/spb/spb_planningorder/get/"+ data)
    // }
    // get(id) {
    //   return spborder.get(`/tutorials/${id}`);
    // }
    // signViaAdminSc(data) {
    //   return spborder.post("adm/auth/signviaadminsc", data);
    // }
    // update(id, data) {
    //   return spborder.put(`/tutorials/${id}`, data);
    // }
    // delete(id) {
    //   return spborder.delete(`/tutorials/${id}`);
    // }
    // deleteAll() {
    //   return spborder.delete(`/tutorials`);
    // }
    // findByTitle(title) {
    //   return spborder.get(`/tutorials?title=${title}`);
    // }
  }
  export default new PlanningOrder();