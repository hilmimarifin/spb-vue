import spbAdminApi from "./api/SpbAdminApi";

class NotificationService {
    getModulType() {
        return spbAdminApi.get('spb/notification/getallmodules');
    }
}
export default new NotificationService();
